const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://mongoadmin:admin@cluster0.sao0e.mongodb.net/Capstone2tagudin152?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("Connected to MongoDB"));

app.use(cors());
app.use(express.json());

const userRoutes = require("./routes/userRoutes");
app.use('/users', userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products', productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use('/orders', orderRoutes);

app.listen(port, () => console.log(`running on ${port}`));