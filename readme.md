Project Name: E-commerce API

Admin Credentials:
	email: "admin@gmail.com"
	password: "admin123"

Regular User Credentials:
	email: "sampleUser@gmail.com"
	password: "sample123"



Features:
User Registration
User Authentication
Set user as Admin (Admin only)
Retrieve all active products
Retrieve single product
Create Product (Admin only)
Update Product Information (Admin only)
Archive Product (Admin only)
Non-admin user Checkout (create order)
Retrieve authenticated user's orders
Retrieve all orders (Admin only)
Activate Product (Admin only)
View products per user order
Search Products
LessThan Price Filter



Heroku
https://peaceful-anchorage-75608.herokuapp.com/ | https://git.heroku.com/peaceful-anchorage-75608.git
