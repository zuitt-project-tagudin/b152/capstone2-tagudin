const Order = require("../models/Order");
const Product = require("../models/Product");


module.exports.createOrder = (req, res) => {

	if (req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	

	let newOrder = new Order({
		userId: req.user.id,
		totalAmount: req.body.totalAmount,
		products: req.body.products

	})

	newOrder.save();

	//console.log(newOrder);

	let newOrderId = newOrder.id;

	//console.log(orderId);

	req.body.products.forEach((product) => {
		//console.log(product.productId);

		Product.findById(product.productId)
		.then(xxxx => {
			let order = {
				orderId: newOrderId,
				quantity: product.quantity
			}
			xxxx.orders.push(order);

			xxxx.save()
		})
	})

	return res.send({message: "Orders added to products"});
	//cant figure out how to throw an error in case; dont know where to put in a catch in case of typos, etc.
}

module.exports.getMyOrders = (req, res) => {

	console.log(req.user.id);
	Order.find({userId: req.user.id})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getAllOrders = (req, res) => {
	
	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.seeProducts = (req, res) => {

	Order.findById(req.params.id)
	.then(result => res.send(result.products))
	.catch(err => res.send(err));

}