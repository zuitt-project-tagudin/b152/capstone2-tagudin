const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.register = (req, res) => {


	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));

}

module.exports.login = (req, res) => {

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if (foundUser === null) {
			return res.send("User does not exist. Please sign up")
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send({message: "Wrong Password"})
			}
		}

	})
	.catch(err => res.send(err))

}

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}


module.exports.makeAdmin = (req, res) => {

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}


module.exports.getUserDetails = (req, res) => {

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));

	//res.send("testing for verify")
}