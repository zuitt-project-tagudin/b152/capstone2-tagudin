const Product = require("../models/Product");


module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		url: req.body.url
	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(err => res.send(err));

}

module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getActiveProducts = (req, res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getSingleProduct = (req, res) => {

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.updateProduct = (req, res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate({_id: req.params.id}, updates, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.archive = (req, res) => {

	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.activate = (req, res) => {

	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

//find products by having a string as an input (search bar probably from frontend side). case insensitive. can be searched by anyone.
module.exports.findByName = (req, res) => {

	Product.find({name: {$regex: req.body.name, $options: '$i'}, isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}


//find products by having a number as an input. will display products less than or equal to the number received.
module.exports.lessThanThis = (req, res) => {

	Product.find({price: {$lte: req.body.price}})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}