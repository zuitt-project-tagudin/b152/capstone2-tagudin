const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const userControllers = require("../controllers/userControllers");


router.post('/', userControllers.register);

router.post('/login', userControllers.login);

router.get('/', userControllers.getAllUsers);

router.put("/makeAdmin/:id", verify, verifyAdmin, userControllers.makeAdmin);

router.get("/getUserDetails", verify, userControllers.getUserDetails);

module.exports = router;