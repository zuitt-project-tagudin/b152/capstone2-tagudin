const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const productControllers = require("../controllers/productControllers");

router.post('/', verify, verifyAdmin, productControllers.addProduct);

router.get('/', productControllers.getAllProducts);

router.get('/getActiveProducts', productControllers.getActiveProducts);

router.get('/getSingleProduct/:id', productControllers.getSingleProduct);

router.put('/updateProduct/:id', verify, verifyAdmin, productControllers.updateProduct);

router.put('/archive/:id', verify, verifyAdmin, productControllers.archive);

router.put('/activate/:id', verify, verifyAdmin, productControllers.activate)

router.post('/findByName', productControllers.findByName);

router.post('/lessThanThis', productControllers.lessThanThis);


module.exports = router;