const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const orderControllers = require("../controllers/orderControllers");

router.post('/', verify, orderControllers.createOrder);

router.get('/getMyOrders', verify, orderControllers.getMyOrders);

router.get('/getAllOrders', verify, verifyAdmin, orderControllers.getAllOrders);

router.get('/seeProducts/:id', verify, orderControllers.seeProducts);

module.exports = router;