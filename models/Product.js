const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product Name is Required"]
	},
	description: {
		type: String,
		required: [true, "Description is Required"]
	},
	url: {
		type: String,
		required: [true, "Image URL is Required"]
	},
	price: {
		type: Number,
		required: [true, "Price is Required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "UserID is required"]

			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
	
})

module.exports = mongoose.model("Product", productSchema);