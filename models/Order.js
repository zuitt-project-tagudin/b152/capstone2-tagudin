const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "UserID is required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total Amount required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "ProductID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);